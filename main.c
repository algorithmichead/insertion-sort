#include <stdlib.h>
#include <stdio.h>

#define ARRAY_MAX_LEN 100

int main()
{
   int array[ARRAY_MAX_LEN];
   int arrayLen;

   printf("\nInsertion Sort by algorithmichead\n");
   printf("algorithmichead.com\n");

   printf("Enter max entry number (Max %d):\n",
           ARRAY_MAX_LEN);
   scanf("%d",&arrayLen);
   if(arrayLen>ARRAY_MAX_LEN)
   {
      printf("Max array size is %d!!!\n",ARRAY_MAX_LEN);
      exit(1);
   }

   int i,temp,j;
   int repeat=1;

   for(i=0;i<arrayLen;i++)
   {
      printf("Enter %d. number:\n",i+1);
      scanf("%d",&array[i]);
   }

   for(i=1;i<arrayLen;i++)
   {
      temp=array[i];
      j=i-1;
      while(j>=0 && array[j]>temp)
      {
         array[j+1]=array[j];
         j--;
      }
   array[j+1]=temp;
   }

   printf("Sorted array:\n");
   for(i=0;i<arrayLen;i++)
   {
      printf("%d  ",array[i]);
   }

   printf("\n");
   return 0;
} 
